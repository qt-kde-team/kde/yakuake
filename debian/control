Source: yakuake
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Norbert Preining <norbert@preining.info>,
           Scarlett Moore <sgmoore@kde.org>,
           Didier Raboud <odyx@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.3.0~),
               gettext,
               kwayland-dev (>= 4:5.240.0~) [linux-any],
               libkf6archive-dev (>= 6.3.0~),
               libkf6config-dev (>= 6.3.0~),
               libkf6coreaddons-dev (>= 6.3.0~),
               libkf6crash-dev (>= 6.3.0~),
               libkf6dbusaddons-dev (>= 6.3.0~),
               libkf6globalaccel-dev (>= 6.3.0~),
               libkf6i18n-dev (>= 6.3.0~),
               libkf6iconthemes-dev (>= 6.3.0~),
               libkf6kio-dev (>= 6.3.0~),
               libkf6newstuff-dev (>= 6.3.0~),
               libkf6notifications-dev (>= 6.3.0~),
               libkf6notifyconfig-dev (>= 6.3.0~),
               libkf6parts-dev (>= 6.3.0~),
               libkf6statusnotifieritem-dev (>= 6.3.0~),
               libkf6widgetsaddons-dev (>= 6.3.0~),
               libkf6windowsystem-dev (>= 6.3.0~),
               libx11-dev,
               qt6-base-dev (>= 6.5.0),
               qt6-svg-dev (>= 6.5.0),
Standards-Version: 4.7.0
Homepage: https://apps.kde.org/en/yakuake
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/yakuake.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/yakuake
Rules-Requires-Root: no

Package: yakuake
Architecture: any
Depends: konsole-kpart (>= 24.08~), ${misc:Depends}, ${shlibs:Depends},
Provides: x-terminal-emulator,
Description: Quake-style terminal emulator based on KDE Konsole technology
 YaKuake is inspired from the terminal in the Quake game: when you press a key
 (by default F12, but that can be changed) a terminal window slides down from
 the top of the screen. Press the key again, and the terminal slides back.
 .
 It is faster than a keyboard shortcut because it is already loaded into memory
 and as such is very useful to anyone who frequently finds themselves switching
 in and out of terminal sessions.
